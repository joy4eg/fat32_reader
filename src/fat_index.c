#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

#include "fat.h"
#include "fat_index.h"
#include "utils.h"

enum fat_parse_action {
	ACTION_STOP = 0,
	ACTION_NEXT,
	ACTION_DIR,
	ACTION_LFN,
};

struct fat_chain_content {
	union {
		void    *ptr;
		uint32_t cluster;
	} dir;
	struct {
		char value[255];
		int len;
	} name;
};

static int
fat_parse_long_name_entry(char *dest, const uint16_t *buf, int len)
{
	int i, n = 0;

	for (i = 0; i < len; i++) {
		if (buf[i] == '\0' || buf[i] == 0xFFFF)
			break;
		dest[n++] = (char)buf[i];
	}

	return n;
}

static int
fat_parse_long_name(const struct fat32_file_ex *file, char *buf)
{
	int p = 0;

	p += fat_parse_long_name_entry(buf + p, file->name_0, ARRAY_SIZE(file->name_0));
	p += fat_parse_long_name_entry(buf + p, file->name_1, ARRAY_SIZE(file->name_1));
	p += fat_parse_long_name_entry(buf + p, file->name_2, ARRAY_SIZE(file->name_2));

	return p;
}

static void
fat_parse_short_name(char *name)
{
	int i;
	const int fat32_short_name_len = 11;

	for (i = fat32_short_name_len - 1; i >= 0 && name[i] == ' '; i--) {
		name[i] = '\0';
	}
}

static enum fat_parse_action
fat_index_parse_chain(struct fat_handle *handle, uint32_t cluster, uint32_t sector, uint32_t chain, struct fat_chain_content *content)
{
	union {
		uint8_t              raw[32];
		struct fat32_file_ex file_ex;
		struct fat32_file    file;
	} entry = { .raw = { 0 } };
	uint8_t attr;
	off_t offset = fat_sector_to_offset(handle, cluster, sector) + chain;

	/* Read raw entry */
	fat_read(handle, entry.raw, offset, sizeof(entry.raw));

	switch (entry.raw[0]) {
		case '\0':              /* Free entry */
			return ACTION_STOP;
		case 0xe5:              /* Deleted entry */
			return ACTION_NEXT;
	}

	/* Read attr section */
	attr = entry.raw[offsetof(struct fat32_file, attr)];

	/* Parse name, type ... */
	if (attr == FAT32_FILE_ATTR_LFN) {
		content->name.len = fat_parse_long_name(&entry.file_ex, content->name.value);
		return ACTION_LFN;
	}
	else {
		strlcpy(content->name.value, entry.file.name, sizeof(content->name.value));
		fat_parse_short_name(content->name.value);
		content->name.len = strlen(content->name.value);
	}

	if (attr & FAT32_FILE_ATTR_VOLUME_ID) {
		/* Skip it for now */
		return ACTION_NEXT;
	}
	else if (attr & FAT32_FILE_ATTR_DIRECTORY) {
		content->dir.cluster = entry.file.cluster_low;
		return ACTION_DIR;
	}

	return ACTION_NEXT;
}

static enum fat_parse_action
fat_index_parse_sector(struct fat_handle *handle, uint32_t cluster, uint32_t sector)
{
	char name[256] = { '\0' };
	enum fat_parse_action prev = ACTION_STOP;
	bool end = false;

	for (uint32_t chain = 0; chain < handle->sb->bytes_per_sector; chain += 32) {
		struct fat_chain_content content = {
			.name = { .len = 0, .value = { '\0' } },
			.dir =  { .cluster = 0 },
		};
		enum fat_parse_action action;

		action = fat_index_parse_chain(handle, cluster, sector, chain, &content);
		if (prev == ACTION_LFN && action != ACTION_LFN) {
			if (handle->parse_cb)
				handle->parse_cb(handle, handle->depth, name, action == ACTION_DIR);
		}
		switch (action) {
			case ACTION_LFN: {
				if (content.name.len > 0) {
					prepend(name, content.name.value);
				}
				break;
			}
			case ACTION_DIR: {
				const char *n = *name ? name : content.name.value;

				/* Skip '.' and '..' */
				if (strcmp(n, ".") == 0 || strcmp(n, "..") == 0) {
					break;
				}
				handle->depth += 2;
				fat_index_parse_cluster_chain(handle, content.dir.cluster);
				handle->depth -= 2;
				break;
			}
			case ACTION_STOP: {
				end = true;
				goto out;
			}
			case ACTION_NEXT: {
				break;
			}
		}
		if (prev == ACTION_LFN && action != ACTION_LFN) {
			memset(name, 0, sizeof(name));
		}
		prev = action;
	}

out:
	return end ? ACTION_STOP : ACTION_NEXT;
}

static enum fat_parse_action
fat_index_parse_cluster(struct fat_handle *handle, uint32_t cluster)
{
	for (uint32_t sector = 0; sector < handle->sb->sectors_per_cluster; sector++) {
		int action = fat_index_parse_sector(handle, cluster, sector);
		if (action == ACTION_STOP)
			break;
	}

	return ACTION_STOP;
}

void
fat_index_parse_cluster_chain(struct fat_handle *handle, uint32_t start_cluster)
{
	uint32_t cluster = start_cluster;
	do {
		fat_index_parse_cluster(handle, cluster);
	} while (fat_cluster_next(handle, &cluster));
}
