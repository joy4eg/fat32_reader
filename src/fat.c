#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include "fat.h"

void
fat_print_volume_info(const struct fat_handle *h)
{
	const struct fat32_sb *sb = h->sb;

	printf("sb [%p] --->\n", sb);
	printf("  OEM version:          %s\n", sb->os_version);
	printf("  Drive number:         %u\n", sb->drive_number);
	printf("  Volume ID:            %#x\n", sb->volume_id);
	printf("  Volume label:         %.*s\n", (int)sizeof(sb->volume_label), (const char*)sb->volume_label);
	printf("  FAT size:             %u\n", sb->table_size_32);
	printf("  FAT count:            %u\n", sb->table_count);
	printf("  Reserved sectors:     %u\n", sb->reserved_sector_count);
	printf("  Sectors per cluster:  %u\n", sb->sectors_per_cluster);
	printf("  Bytes per sector:     %u\n", sb->bytes_per_sector);
	printf("  Total sectors:        %u\n", sb->total_sectors_32);
	printf("  Root cluster:         %u\n", sb->root_cluster);
	printf("  Hidden sectors:       %u\n", sb->hidden_sector_count);
	printf("<--- sb [%p]\n", sb);
}

int
fat_open(struct fat_handle *h, const char *path)
{
	int fd;
	int rc = 0;
	struct stat sb;
	uint8_t *addr;

	fd = open(path, O_RDONLY);
	if (fd < 0) {
		de("cannot open [%s] for reading: %s", path, strerror(errno));
		rc = -1;
		goto err;
	}

	if (fstat(fd, &sb) < 0) {
		de("cannot stat on fd [%d]: %s", fd, strerror(errno));
		rc = -2;
		goto err;
	}

	addr = mmap(NULL, sb.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
	if (addr == MAP_FAILED) {
		de("mmap failed: %s", strerror(errno));
		rc = -2;
		goto err;
	}

	h->fd = fd;
	h->data = addr;
	h->size = sb.st_size;
	h->depth = 0;
	dd("[%s] opened at fd [%d], data [%p], length [%u]", path, fd, h->data, h->size);
	h->sb = (void*)addr;
	return 0;

err:
	close(fd);
	return rc;
}

int
fat_close(struct fat_handle *h)
{
	munmap(h->data, h->size);
	close(h->fd);
	h->data = NULL;
	h->size = 0;
	h->fd = -1;
	return 0;
}
