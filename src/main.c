#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "fat.h"
#include "fat_index.h"
#include "utils.h"

static void
parse_callback(struct fat_handle *handle, int depth, const char *name, uint8_t is_dir)
{
	char buf[512] = { '\0' };

	handle = handle; /* make GCC happy */
	printf("%s─ %s%s\n",
		strrep(buf, depth, " "),
		name,
		is_dir ? "/" : "");
}

int
main(int argc, char *argv[])
{
	const char *disk = NULL;
	extern const char *__progname;
	struct fat_handle handle = {
		.parse_cb = parse_callback,
	};

	if (argc < 2) {
		printf("Usage: %s <disk image>\n", __progname);
		return 1;
	}
	else {
		disk = argv[1];
	}

	/* Try to open disk image or die */
	if (fat_open(&handle, disk) < 0)
		return 1;

	/* Show volume information */
	fat_print_volume_info(&handle);
	printf("%s:\n", disk);

	/* Parse file tree */
	fat_index_parse_cluster_chain(&handle, handle.sb->root_cluster);

	/* Clean up */
	fat_close(&handle);
	return 0;
}
