#pragma once

#include <stdint.h>
#include <string.h>
#include <stdbool.h>

struct fat32_sb {
	/* Generic FAT block */
	uint8_t   bootcode[3];
	char      os_version[8];
	uint16_t  bytes_per_sector;
	uint8_t   sectors_per_cluster;
	uint16_t  reserved_sector_count;
	uint8_t   table_count;
	uint16_t  root_entry_count;
	uint16_t  total_sectors_16;
	uint8_t   disk_type;
	uint16_t  table_size;
	uint16_t  sectors_per_track;
	uint16_t  head_count;
	uint32_t  hidden_sector_count;
	uint32_t  total_sectors_32;

	/* FAT32 specific */
	uint32_t table_size_32;
	uint16_t extended_flags;
	uint16_t fat_version;
	uint32_t root_cluster;
	uint16_t fat_info;
	uint16_t backup_BS_sector;
	uint8_t  reserved_0[12];
	uint8_t  drive_number;
	uint8_t  reserved_1;
	uint8_t  boot_signature;
	uint32_t volume_id;
	uint8_t  volume_label[11];
	uint8_t fat_type_label[8];
} __attribute__((packed));

#define FAT32_FILE_ATTR_READ_ONLY          0x01u
#define FAT32_FILE_ATTR_HIDDEN             0x02u
#define FAT32_FILE_ATTR_SYSTEM             0x04u
#define FAT32_FILE_ATTR_VOLUME_ID          0x08u
#define FAT32_FILE_ATTR_DIRECTORY          0x10u
#define FAT32_FILE_ATTR_ARCHIVE            0x20u
#define FAT32_FILE_ATTR_LFN                (FAT32_FILE_ATTR_READ_ONLY | FAT32_FILE_ATTR_HIDDEN | FAT32_FILE_ATTR_SYSTEM | FAT32_FILE_ATTR_VOLUME_ID)
#define FAT32_FILE_ATTR_LFN_LAST           0x40u

struct fat32_file {
	char name[11];
	uint8_t  attr;
	uint8_t  reserved_0[8]; /* reserved field + creation/access time */
	uint16_t cluster_high;
	uint8_t  reserved_1[4]; /* modification date/time */
	uint16_t cluster_low;
	uint32_t size;
} __attribute__((packed));

struct fat32_file_ex {
	uint8_t seq;
	uint16_t name_0[5];
	uint8_t attr;
	uint8_t reserved_0;
	uint8_t csum;
	uint16_t name_1[6];
	uint16_t reserved_1;
	uint16_t name_2[2];
} __attribute__((packed));
