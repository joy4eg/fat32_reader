#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

extern const char *__progname;

enum log_message_level {
	LOG_LEVEL_DEBUG = 0,
	LOG_LEVEL_INFO,
	LOG_LEVEL_ERROR,
	LOG_LEVEL_FATAL,
};

static inline const char*
__log_level_to_str(enum log_message_level l)
{
	switch (l) {
		case LOG_LEVEL_DEBUG: return "DBG";
		case LOG_LEVEL_ERROR: return "ERR";
		case LOG_LEVEL_FATAL: return "FTL";
		default:              return "INF";
	}
}

#define __dd(level, fmt, ...) fprintf(stderr, "[%03d][%s][%s]: %.10s: " fmt "\n", getpid(), __progname, __log_level_to_str(level), __func__, ## __VA_ARGS__)

#define dd(fmt, ...) __dd(LOG_LEVEL_DEBUG, fmt, ##__VA_ARGS__)
#define di(fmt, ...) __dd(LOG_LEVEL_INFO,  fmt, ##__VA_ARGS__)
#define de(fmt, ...) __dd(LOG_LEVEL_ERROR, fmt, ##__VA_ARGS__)
#define df(fmt, ...)                                                   \
	do {                                                               \
		__dd(LOG_LEVEL_FATAL, fmt, ##__VA_ARGS__);                     \
		exit(1);                                                       \
	} while (0);
