#pragma once

#include <stdint.h>

#define ARRAY_SIZE(x)  (sizeof(x) / sizeof((x)[0]))

void hex_dump(char *desc, void *addr, int len);
void prepend(char* s, const char* t);
size_t strlcpy(char *dst, const char *src, size_t siz);
const char* strrep(char *buf, int count, char *s);
