#pragma once

#include <stdint.h>
#include <string.h>
#include <stdbool.h>

#include "fat32.h"

struct fat_handle {
	uint8_t *data;
	uint32_t size;
	int fd;
	struct fat32_sb const *sb;
	int depth;
	void(*parse_cb)(struct fat_handle *handle, int depth, const char *name, uint8_t is_dir);
};

static inline off_t
fat_get_table_offset(const struct fat_handle *h)
{
	return h->sb->reserved_sector_count * h->sb->bytes_per_sector;
}

static inline off_t
fat_cluster_to_offset(const struct fat_handle *h, uint32_t cluster)
{
	off_t offset = (cluster - 2) * h->sb->sectors_per_cluster;
	off_t fat_size = h->sb->table_size_32 * h->sb->bytes_per_sector;
	off_t clusters_start = fat_get_table_offset(h) + (fat_size * h->sb->table_count);

	return clusters_start + (h->sb->bytes_per_sector * offset);
}

static inline bool
fat_cluster_next(const struct fat_handle *h, uint32_t *cluster)
{
	off_t fat_offset = fat_get_table_offset(h) + (*cluster * 4);
	uint32_t *ptr = (uint32_t*)(h->data + fat_offset);
	uint32_t val = *ptr & 0x0FFFFFFF;

	/* TODO: handle bad, reserved clusters... */
	switch (val) {
		case 0x00000002 ... 0x0FFFFFF6:
			*cluster = val;
			return true;
	}

	return false;
}

static inline off_t
fat_sector_to_offset(const struct fat_handle *h, uint32_t cluster, uint32_t sector)
{
	return fat_cluster_to_offset(h, cluster) + (h->sb->bytes_per_sector * sector);
}

static inline void
fat_read(const struct fat_handle *h, uint8_t *dest, off_t offset, uint32_t count)
{
	memcpy(dest, h->data + offset, count);
}

int fat_open(struct fat_handle *h, const char *path);
int fat_close(struct fat_handle *h);
void fat_print_volume_info(const struct fat_handle *h);
