# fat32_reader
The implementation of FAT32 filesystem directory parser

### Features
  - Parse filesystem tree
  - Show image structure
  - Easily extensible

### How-to build
```bash
    git clone https://gitlab.com/joy4eg/fat32_reader.git
    cd fat32_reader
    mkdir build
    cd build
    cmake ..
    make
```

### References
  - http://elm-chan.org/docs/fat_e.html
  - https://wiki.osdev.org/FAT
  - https://www.pjrc.com/tech/8051/ide/fat32.html

### License
Apache 2.0

